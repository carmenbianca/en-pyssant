# -*- coding: utf-8 -*-
#
# Copyright (C) 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This file is part of En Pyssant, available from its original location:
# <https://gitlab.com/carmenbianca/en-pyssant>.
#
# En Pyssant is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""Create mcts.pickle."""

# pylint: disable=blacklisted-name,missing-docstring,protected-access
# pylint: disable=unused-argument

import logging
import pickle
import signal
import sys
import threading
import time

from en_pyssant.engine import MCTSEngine


def node_count(node):
    result = 0
    for child in node.children:
        result += node_count(child)
    return result + 1


def ignore(*args):
    pass


def kill(*args):
    sys.exit(1)


def main():
    logging.basicConfig(level=logging.DEBUG)
    engine = MCTSEngine(load_precomputed=True)
    thread = threading.Thread(target=engine.think_for, args=(0,))
    thread.start()
    while True:
        signal.signal(signal.SIGINT, kill)
        time.sleep(60)
        signal.signal(signal.SIGINT, ignore)
        with engine._state_lock:
            start = time.perf_counter()
            logging.debug('started writing to mcts.pickle')
            with open('src/en_pyssant/mcts.pickle', 'wb') as fp:
                pickle.dump(engine._root, fp)
            logging.debug(
                'finished writing to mcts.pickle in %f seconds',
                time.perf_counter() - start)
    engine.stop_thinking()
    thread.join()


if __name__ == '__main__':
    main()
