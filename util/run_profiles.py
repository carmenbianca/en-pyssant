# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This file is part of En Pyssant, available from its original location:
# <https://gitlab.com/carmenbianca/en-pyssant>.
#
# En Pyssant is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""Run profiler on games."""

import contextlib
import cProfile
import pstats
import subprocess
import sys
import traceback
from pathlib import Path

from single_game import play_game
from en_pyssant._board import all_board_classes


def main():
    """Run profile against all boards."""
    with contextlib.suppress(FileExistsError):
        Path('prof').mkdir()
    python_version = '{}{}'.format(
        sys.implementation.name,
        '{}.{}.{}'.format(*sys.implementation.version))

    for board_cls in all_board_classes():
        name = '{}-{}'.format(python_version, board_cls.__name__)
        print(name)
        print()

        # Collect profile data.
        stats_file = 'prof/{}.stats'.format(name)
        profile = cProfile.Profile()
        profile.enable()
        play_game(board_cls)
        profile.create_stats()
        profile.dump_stats(stats_file)

        stats = pstats.Stats(stats_file)
        # Print stats for the user.
        stats.sort_stats('tottime').print_stats(30)

        # Generate SVG file.
        try:
            gprof2dot_process = subprocess.Popen(
                ['gprof2dot', '-f', 'pstats', stats_file],
                stdout=subprocess.PIPE)
            dot_process = subprocess.Popen(
                ['dot', '-T', 'svg', '-o', stats_file.replace('stats', 'svg')],
                stdin=gprof2dot_process.stdout)
            gprof2dot_process.stdout.close()
            dot_process.communicate()
        except OSError:
            print(traceback.format_exc())


if __name__ == '__main__':
    main()
