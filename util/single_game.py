# -*- coding: utf-8 -*-
#
# Copyright (C) 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This file is part of En Pyssant, available from its original location:
# <https://gitlab.com/carmenbianca/en-pyssant>.
#
# En Pyssant is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""Play a single game."""

import argparse

from en_pyssant import Game
from en_pyssant._board import all_board_classes

PGN = (
    '[Event "F/S Return Match"]\n'
    '[Site "Belgrade, Serbia JUG"]\n'
    '[Date "1992.11.04"]\n'
    '[Round "29"]\n'
    '[White "Fischer, Robert J."]\n'
    '[Black "Spassky, Boris V."]\n'
    '[Result "1/2-1/2"]\n'
    '\n'
    '1. e4 e5 2. Nf3 Nc6 3. Bb5 a6 4. Ba4 Nf6 5. O-O Be7 6. Re1 b5\n'
    '7. Bb3 d6 8. c3 O-O 9. h3 Nb8 10. d4 Nbd7 11. c4 c6 12. cxb5 axb5\n'
    '13. Nc3 Bb7 14. Bg5 b4 15.  Nb1 h6 16. Bh4 c5 17. dxe5 Nxe4\n'
    '18. Bxe7 Qxe7 19. exd6 Qf6 20. Nbd2 Nxd6 21.  Nc4 Nxc4 22. Bxc4 Nb6\n'
    '23. Ne5 Rae8 24. Bxf7+ Rxf7 25. Nxf7 Rxe1+ 26. Qxe1 Kxf7 27. Qe3\n'
    'Qg5 28. Qxg5 hxg5 29. b3 Ke6 30. a3 Kd6 31. axb4 cxb4 32. Ra5 Nd5\n'
    '33.  f3 Bc8 34. Kf2 Bf5 35. Ra7 g6 36. Ra6+ Kc5 37. Ke1 Nf4 38. g3\n'
    'Nxh3 39. Kd2 Kb5 40. Rd6 Kc5 41. Ra6 Nf2 42. g4 Bd3 43. Re6\n'
    '1/2-1/2\n')


def play_game(board_cls):
    """Play a single game using *board_cls*."""
    Game.from_pgn(PGN, board_cls=board_cls)


def main():
    """Main function."""
    names = {board.__name__: board for board in all_board_classes()}
    parser = argparse.ArgumentParser(description='Play a single game')
    parser.add_argument(
        '--board', choices=list(names), default='DictBoard',
        help='Board to use')

    parsed = parser.parse_args()
    board_cls = names[parsed.board]

    play_game(board_cls)


if __name__ == '__main__':
    main()
