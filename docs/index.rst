..
  Copyright (C) 2017  Carmen Bianca Bakker <carmen@carmenbianca.eu>

  This file is part of En Pyssant, available from its original location:
  <https://gitlab.com/carmenbianca/en-pyssant>.

  This work is licensed under the Creative Commons Attribution-ShareAlike
  4.0 International License. To view a copy of this license, visit
  <http://creativecommons.org/licenses/by-sa/4.0/>.

  SPDX-License-Identifier: CC-BY-SA-4.0

Welcome to En Pyssant's documentation!
======================================

.. toctree::
   :maxdepth: 2

   readme
   authors
   changelog
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
