..
  Copyright (C) 2017-2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>

  This file is part of En Pyssant, available from its original location:
  <https://gitlab.com/carmenbianca/en-pyssant>.

  This work is licensed under the Creative Commons Attribution-ShareAlike
  4.0 International License. To view a copy of this license, visit
  <http://creativecommons.org/licenses/by-sa/4.0/>.

  SPDX-License-Identifier: CC-BY-SA-4.0

==========
Change log
==========

This change log follows the `Keep a Changelog <http://keepachangelog.com/>`_
spec.  Every release contains the following sections:

- *Added* for new features.

- *Changed* for changes in existing functionality.

- *Deprecated* for soon-to-be removed features.

- *Removed* for now removed features.

- *Fixed* for any bug fixes.

- *Security* in case of vulnerabilities.

The versions follow `semantic versioning <https://semver.org>`_.


0.2.1 (2018-07-04)
==================

Changed
-------

- Updated versions in requirements.txt.

- Added Python 3.7 to supported versions.

- Made sure lint and docs are run on Python 3.6.


0.2.0 (2018-07-04)
==================

Added
-----

- ``moves_single`` now complements ``moves`` as a function that generates all
  legal moves for a single origin square.

- ``BitBoard`` and ``TupleBoard`` added.

- Added ``Piece.from_str``.

- Added ``do_move_with_history``, which returns a ``(Position, HistoryRecord)``
  tuple.

- ``ParallelEngine`` (base class) and ``MCTSEngine`` added.  There is now a
  fully functional engine that isn't ``RandomEngine``.

- Added more methods to ``Engine``.

Changed
-------

- ``CastlingAvailability`` has been replaced with ``CastlingSide``.  Positions
  now no longer contain a dictionary of ``CastlingAvailability`` objects, but a
  ``Castling`` object.  For example:

  ``castling = {Side.WHITE: CastlingAvailability(True, True), Side.BLACK:
  CastlingAvailability(True, True)``

  is now

  ``castling = Castling(CastlingSide(True, True), CastlingSide(True, True))``

  In effect, this makes ``Position`` objects hashable and entirely immutable.

  The new ``Castling`` class still permits key lookup.  So
  ``castling[Side.WHITE].kingside`` remains valid.

- ``Square.in_bounds`` and ``Square.goto`` now also accept ``(int, int)`` tuples
  in lieue of ``Direction`` objects.  This is more performant because tuples
  hash quicker.

- ``Board.all_pieces`` now starts at a1 and goes to h8.

- Changed some code around to be more
  ``threading``/``multiprocessing``-friendly.

- Changed the public interface of ``Engine``.  Specifically:

  + ``Engine.__init__`` now takes a position, history and ruleset instead of a
    game.

  + ``Engine.think_for`` returns True.  If another thread is already thinking,
    it returns False and does not begin thinking.

  + ``Engine.stop_thinking`` takes an additional ``blocking`` keyword argument.


0.1.7 (2018-04-02)
==================

Changed
-------

- Re-release to fix the documentation.  No changes in the codebase.


0.1.5 (2018-03-13)
==================

- First release.

- Contains almost all functionality except the chess engine itself.  You can
  play chess, basically.  Just not against a hyper-intelligent computer.
