# Copyright (C) 2017-2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
#
# This file is part of En Pyssant, available from its original location:
# <https://gitlab.com/carmenbianca/en-pyssant>.
#
# En Pyssant is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

.PHONY: clean
clean: clean-build clean-pyc clean-test clean-docs ## remove all build, test, coverage and Python artifacts

.PHONY: clean-build
clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -fr {} +

.PHONY: clean-pyc
clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.PHONY: clean-test
clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage*
	rm -fr htmlcov/
	rm -fr .benchmarks/
	rm -fr prof/

.PHONY: clean-docs
clean-docs: ## remove docs build artifacts
	-$(MAKE) -C docs clean
	rm -f docs/en_pyssant*.rst
	rm -f docs/modules.rst

.PHONY: pylint
pylint: ## check style with pylint
	pylint src/en_pyssant tests util/*.py

.PHONY: flake8
flake8: ## check style with flake8
	flake8 src/ tests/ util/ setup.py

.PHONY: reuse
reuse:  ## check for REUSE compliance
	reuse lint

.PHONY: checkdocs
checkdocs:  ## verify whether the long description passes the PyPI linter
	python setup.py checkdocs

.PHONY: lint
lint: pylint flake8 reuse checkdocs  ## check style with all linters

.PHONY: doctest
doctest:  ## run doctests
	py.test --doctest-modules src/en_pyssant README.rst

.PHONY: test
test: doctest  ## run tests quickly
	py.test

.PHONY: test-all
test-all: ## run tests on every Python version with tox
	tox

.PHONY: coverage
coverage: ## check code coverage quickly
	py.test --doctest-modules --cov-report term-missing --cov=src/en_pyssant src/en_pyssant tests README.rst

.PHONY: benchmark
benchmark: ## run benchmark suite
	py.test --benchmark-only --benchmark-autosave --benchmark-sort='mean' '-m benchmark'

name := $(shell python --version 2>&1)

.PHONY: profile
profile: ## profile a single game for each board
	python util/run_profiles.py

.PHONY: docs
docs: clean-docs ## generate Sphinx HTML documentation, including API docs
	sphinx-apidoc --separate -o docs/ src/en_pyssant
	$(MAKE) -C docs html

.PHONY: test-release
test-release: dist checkdocs  ## package and upload to testpypi
	twine upload --sign -r testpypi dist/*

.PHONY: release
release: dist checkdocs  ## package and upload a release
	twine upload --sign -r pypi dist/*

.PHONY: dist
dist: clean ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

.PHONY: install-requirements
install-requirements:  ## install requirements
	pip install -r requirements.txt

.PHONY: uninstall
uninstall:  ## uninstall en-pyssant
	-pip uninstall -y en-pyssant

.PHONY: install
install: uninstall install-requirements dist  ## install as wheel
	pip install dist/*.whl

.PHONY: develop
develop: uninstall install-requirements  ## install as link to source directory
	python setup.py develop
